#ifndef STREAM9_INDIRECT_INDIRECT_ITERATOR_HPP
#define STREAM9_INDIRECT_INDIRECT_ITERATOR_HPP

#include "namespace.hpp"
#include "redirect.hpp"

#include <iterator>

#include <stream9/iterators.hpp>

namespace stream9::iterators {

template<std::input_iterator T>
class indirect_iterator : public iterator_adaptor<indirect_iterator<T>,
                            T,
                            typename std::iterator_traits<T>::iterator_category,
                            redirect_result_t<std::iter_reference_t<T>> >
{
public:
    indirect_iterator() = default;

    indirect_iterator(T const it) noexcept
        : indirect_iterator<T>::adaptor_type { it }
    {}

    template<std::convertible_to<T> U>
    indirect_iterator(indirect_iterator<U> const it) noexcept
        : indirect_iterator<T>::adaptor_type { it.base() }
    {}

private:
    friend class iterator_core_access;

    indirect_iterator<T>::adaptor_type::reference
    dereference() const noexcept
    {
        return redirect(*this->base());
    }
};

namespace indirect_ {

    template<typename> struct add_const;

    template<typename T>
    struct add_const
    {
        using type = T const;
    };

    template<typename T>
    struct add_const<T&>
    {
        using type = T const&;
    };

    template<typename T>
    using add_const_t = add_const<T>::type;

} // namespace indirect_

template<std::input_iterator T>
class const_indirect_iterator
    : public iterator_adaptor<const_indirect_iterator<T>,
             T,
             typename std::iterator_traits<T>::iterator_category,
             indirect_::add_const_t<
                 redirect_result_t<std::iter_reference_t<T>> >>
{
public:
    const_indirect_iterator() = default;

    const_indirect_iterator(T const it) noexcept
        : const_indirect_iterator<T>::adaptor_type { it }
    {}

    template<std::convertible_to<T> U>
    const_indirect_iterator(const_indirect_iterator<U> const it) noexcept
        : const_indirect_iterator<T>::adaptor_type { it.base() }
    {}

    template<std::convertible_to<T> U>
    const_indirect_iterator(indirect_iterator<U> const it) noexcept
        : const_indirect_iterator<T>::adaptor_type { it.base() }
    {}

private:
    friend class iterator_core_access;

    const_indirect_iterator<T>::adaptor_type::reference
    dereference() const noexcept
    {
        return redirect(*this->base());
    }
};

} // namespace stream9::iterators

#endif // STREAM9_INDIRECT_INDIRECT_ITERATOR_HPP
