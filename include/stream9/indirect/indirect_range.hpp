#ifndef STREAM9_INDIRECT_INDIRECT_RANGE_HPP
#define STREAM9_INDIRECT_INDIRECT_RANGE_HPP

#include "namespace.hpp"
#include "indirect_iterator.hpp"

#include <ranges>
#include <utility>

namespace stream9::ranges::views {

namespace indirect_ {

    namespace rng = std::ranges;
    namespace iter = stream9::iterators;

    struct api {
        template<rng::range R>
        auto
        operator()(R&& r) const noexcept
        {
            return rng::subrange {
                iter::indirect_iterator { rng::begin(std::forward<R>(r)) },
                iter::indirect_iterator { rng::end(std::forward<R>(r)) }
            };
        }
    };

} // namespace indirect_

inline constexpr indirect_::api indirect;

namespace const_indirect_ {

    namespace rng = std::ranges;
    namespace iter = stream9::iterators;

    struct api {
        template<rng::range R>
        auto
        operator()(R&& r) const noexcept
        {
            return rng::subrange {
                iter::const_indirect_iterator { rng::begin(std::forward<R>(r)) },
                iter::const_indirect_iterator { rng::end(std::forward<R>(r)) }
            };
        }
    };

} // namespace const_indirect_

inline constexpr const_indirect_::api const_indirect;

} // namespace stream9::ranges::views

#endif // STREAM9_INDIRECT_INDIRECT_RANGE_HPP
