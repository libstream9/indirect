#ifndef STREAM9_INDIRECT_REDIRECT_HPP
#define STREAM9_INDIRECT_REDIRECT_HPP

#include "namespace.hpp"

#include <stream9/tag_invoke.hpp>

#include <functional>
#include <iterator>
#include <memory>
#include <optional>
#include <utility>

namespace stream9 {

template<typename> class node;
template<typename> class shared_node;

} // namespace stream9

namespace stream9::indirects {

struct redirect_tag {};

template<typename T>
constexpr decltype(auto)
tag_invoke(redirect_tag, T* const v) noexcept
{
    return *v;
}

template<typename T>
constexpr decltype(auto)
tag_invoke(redirect_tag, std::unique_ptr<T> const& v) noexcept
{
    return *v;
}

template<typename T>
constexpr decltype(auto)
tag_invoke(redirect_tag, std::shared_ptr<T> const& v) noexcept
{
    return *v;
}

template<typename T>
constexpr decltype(auto)
tag_invoke(redirect_tag, std::reference_wrapper<T> const v) noexcept
{
    return v.get();
}

template<std::input_or_output_iterator T>
constexpr decltype(auto)
tag_invoke(redirect_tag, T const& v) noexcept
{
    return *v;
}

template<typename T>
constexpr decltype(auto)
tag_invoke(redirect_tag, std::optional<T>& v) noexcept
{
    return *v;
}

template<typename T>
constexpr decltype(auto)
tag_invoke(redirect_tag, std::optional<T> const& v) noexcept
{
    return *v;
}

template<typename T>
constexpr decltype(auto)
tag_invoke(redirect_tag, node<T> const& v) noexcept
{
    return *v;
}

template<typename T>
constexpr decltype(auto)
tag_invoke(redirect_tag, shared_node<T> const& v) noexcept
{
    return *v;
}

} // namespace stream9::indirects

namespace stream9 {

template<typename T>
concept redirectable = tag_invocable<indirects::redirect_tag, T>;

template<typename T>
concept nothrow_redirectable = nothrow_tag_invocable<indirects::redirect_tag, T>;

template<redirectable T>
constexpr decltype(auto)
redirect(T&& v)
    noexcept(nothrow_redirectable<T>)
{
    return stream9::tag_invoke(indirects::redirect_tag{}, v);
}

template<typename T>
using redirect_result_t = decltype(redirect(std::declval<T>()));

} // namespace stream9

#endif // STREAM9_INDIRECT_REDIRECT_HPP
