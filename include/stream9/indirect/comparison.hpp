#ifndef STREAM9_INDIRECT_COMPARISON_HPP
#define STREAM9_INDIRECT_COMPARISON_HPP

#include "namespace.hpp"
#include "redirect.hpp"

#include <concepts>
#include <utility>

namespace stream9::indirect {

struct equal_to
{
    template<redirectable T, redirectable U>
        requires std::equality_comparable_with<
                        redirect_result_t<T>, redirect_result_t<U> >
    constexpr bool
    operator()(T&& t, U&& u) const
        noexcept(noexcept((redirect)(std::forward<T>(t)) ==
                          (redirect)(std::forward<U>(u)) ))
    {
        return (redirect)(std::forward<T>(t)) == (redirect)(std::forward<U>(u));
    }
};

struct not_equal_to
{
    template<redirectable T, redirectable U>
        requires std::equality_comparable_with<
                        redirect_result_t<T>, redirect_result_t<U> >
    constexpr bool
    operator()(T&& t, U&& u) const
        noexcept(noexcept(!equal_to{}(std::forward<T>(t), std::forward<U>(u))))
    {
        return !equal_to{}(std::forward<T>(t), std::forward<U>(u));
    }
};

struct less
{
    template<redirectable T, redirectable U>
        requires std::totally_ordered_with<
                        redirect_result_t<T>, redirect_result_t<U> >
    constexpr bool
    operator()(T&& t, U&& u) const
        noexcept(noexcept((redirect)(std::forward<T>(t)) <
                          (redirect)(std::forward<U>(u)) ))
    {
        return (redirect)(std::forward<T>(t)) < (redirect)(std::forward<U>(u));
    }
};

struct greater
{
    template<redirectable T, redirectable U>
        requires std::totally_ordered_with<
                        redirect_result_t<T>, redirect_result_t<U> >
    constexpr bool
    operator()(T&& t, U&& u) const
        noexcept(noexcept(less{}(std::forward<U>(u), std::forward<T>(t))))
    {
        return less{}(std::forward<U>(u), std::forward<T>(t));
    }
};

struct less_equal
{
    template<redirectable T, redirectable U>
        requires std::totally_ordered_with<
                        redirect_result_t<T>, redirect_result_t<U> >
    constexpr bool
    operator()(T&& t, U&& u) const
        noexcept(noexcept(!less{}(std::forward<U>(u), std::forward<T>(t))))
    {
        return !less{}(std::forward<U>(u), std::forward<T>(t));
    }
};

struct greater_equal
{
    template<redirectable T, redirectable U>
        requires std::totally_ordered_with<
                        redirect_result_t<T>, redirect_result_t<U> >
    constexpr bool
    operator()(T&& t, U&& u) const
        noexcept(noexcept(!less{}(std::forward<T>(t), std::forward<U>(u))))
    {
        return !less{}(std::forward<T>(t), std::forward<U>(u));
    }
};

struct compare_three_way
{
    template<redirectable T, redirectable U>
        requires std::three_way_comparable_with<
                        redirect_result_t<T>, redirect_result_t<U> >
    constexpr auto
    operator()(T&& t, U&& u) const
        noexcept(noexcept((redirect)(std::forward<T>(t)) <=>
                          (redirect)(std::forward<U>(u)) ))
    {
        return (redirect)(std::forward<T>(t)) <=> (redirect)(std::forward<U>(u));
    }
};

} // namespace stream9::indirect

#endif // STREAM9_INDIRECT_COMPARISON_HPP
