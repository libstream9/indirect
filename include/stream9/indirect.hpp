#ifndef STREAM9_INDIRECT_HPP
#define STREAM9_INDIRECT_HPP

#include <utility>

namespace stream9 {

template<typename T>
struct indirect : T
{
    using T::T;
    using T::operator=;

    template<typename... Args>
    decltype(auto)
    operator()(Args&&... args) const
        noexcept(noexcept(std::declval<T>()(*std::forward<Args>(args)...)))
        requires requires (T fn, Args&&... args) {
            fn(*std::forward<Args>(args)...);
        }
    {
        return T::operator()(*std::forward<Args>(args)...);
    }
};

} // namespace stream9

/*
 * namespace: stream9
 * concept: redirectable, nothrow_redirectable
 * function: redirect
 * customization tag: redirect_tag
 * type function: redirect_result_t
 */
#include "indirect/redirect.hpp"

/*
 * namespace: stream9::iterators
 * class: indirect_iterator
 */
#include "indirect/indirect_iterator.hpp"

/*
 * namespace: stream9::ranges::views
 * class: indirect
 */
#include "indirect/indirect_range.hpp"

#endif // STREAM9_INDIRECT_HPP
